package dev.entze.sge.game.dicepoker;

import static dev.entze.sge.game.dicepoker.DicePokerUtil.Rank.FIVE_OF_A_KIND;
import static dev.entze.sge.game.dicepoker.DicePokerUtil.Rank.FOUR_OF_A_KIND;
import static dev.entze.sge.game.dicepoker.DicePokerUtil.Rank.FULL_HOUSE;
import static dev.entze.sge.game.dicepoker.DicePokerUtil.Rank.NOTHING;
import static dev.entze.sge.game.dicepoker.DicePokerUtil.Rank.PAIR;
import static dev.entze.sge.game.dicepoker.DicePokerUtil.Rank.STRAIGHT;
import static dev.entze.sge.game.dicepoker.DicePokerUtil.Rank.THREE_OF_A_KIND;
import static dev.entze.sge.game.dicepoker.DicePokerUtil.Rank.TWO_PAIRS;

import at.ac.tuwien.ifs.sge.util.Util;
import java.util.Arrays;
import java.util.Comparator;

public class DicePokerUtil {

  public static void checkHand(int[] faces) {
    String errorMsg = "";
    if (faces.length != 5) {
      errorMsg = "Need 5 dice";
    }

    if (!Arrays.stream(faces).allMatch(f -> 1 <= f && f <= 6)) {
      if (!errorMsg.isEmpty()) {
        errorMsg += ", ";
      }
      errorMsg += "Not all faces are valid";
    }

    if (!errorMsg.isBlank()) {
      throw new IllegalArgumentException(errorMsg);
    }
  }

  public static String labelOfHand(int[] faces) {
    checkHand(faces);
    Util.sort(
        faces, (getDiceFaceComparator(faces).thenComparing(Comparator.naturalOrder())).reversed());

    Rank rank = rankOfHand(faces);

    return labelOfHand(faces, rank);
  }

  private static String labelOfHand(int[] faces, Rank rank) {
    switch (rank) {
      case NOTHING:
        return "Nothing";
      case PAIR:
        return faces[0] + " Pair";
      case TWO_PAIRS:
        return faces[0] + ", " + faces[2] + " Two Pair";
      case THREE_OF_A_KIND:
        return faces[0] + " Three of a Kind";
      case STRAIGHT:
        return faces[0] + "-high Straight";
      case FULL_HOUSE:
        return faces[0] + ", " + faces[3] + " Full House";
      case FOUR_OF_A_KIND:
        return faces[0] + " Four of a Kind";
      case FIVE_OF_A_KIND:
        return faces[0] + " Five of a Kind";
    }

    assert false;
    return null;
  }

  private static Rank rankOfHand(int[] faces) {
    if (faces[0] == faces[1]) { // [x, x, ?, ?, ?]
      if (faces[1] == faces[2]) { // [x, x, x, ?, ?]
        if (faces[2] == faces[3]) { // [x, x, x, x, ?]
          if (faces[3] == faces[4]) { // [x, x, x, x, x]
            return FIVE_OF_A_KIND;
          }
          return FOUR_OF_A_KIND; // [x, x, x, x, y]
        } else if (faces[3] == faces[4]) { // [x, x, x, y, y]
          return FULL_HOUSE;
        }
        return THREE_OF_A_KIND; // [x, x, x, y, z]
      } else if (faces[2] == faces[3]) { // [x, x, y, y, z]
        return TWO_PAIRS;
      }
      return PAIR; // [x, x, y, z, a]
    } else if (faces[0] - 1 == faces[1]
        && faces[1] - 1 == faces[2]
        && faces[2] - 1 == faces[3]
        && faces[3] - 1 == faces[4]) {
      return STRAIGHT;
    }
    return NOTHING; // [6, x, y, z, a]
  }

  public static int rankingOfHand(int[] faces) {
    checkHand(faces);

    Util.sort(
        faces, (getDiceFaceComparator(faces).thenComparing(Comparator.naturalOrder())).reversed());

    Rank rank = rankOfHand(faces);
    return rankingOfHand(faces, rank);
  }

  private static int rankingOfHand(int[] faces, Rank rank) {
    switch (rank) {
      case NOTHING:
        return rank.getRank();
      case PAIR:
      case THREE_OF_A_KIND:
      case FOUR_OF_A_KIND:
      case FIVE_OF_A_KIND:
        return rank.getRank() + faces[0];
      case TWO_PAIRS:
      case FULL_HOUSE:
        return rank.getRank() + ((faces[0] - 2) * (faces[0] - 1)) / 2 + faces[3];
      case STRAIGHT:
        return faces[4];
    }
    assert false;
    return -1;
  }

  public static Comparator<Integer> getDiceFaceComparator(int[] faces) {
    if (faces.length < 5) {
      throw new IllegalArgumentException("Need at least 5 dice");
    }
    return new DiceFaceComparator(faces);
  }

  public static boolean isFullHand(int[] face) {
    return face.length == 5 && Arrays.stream(face).allMatch(f -> 1 <= f && f <= 6);
  }

  private static class DiceFaceComparator implements Comparator<Integer> {

    private int[] frequency;

    public DiceFaceComparator(int[] faces) {
      frequency = new int[Arrays.stream(faces).max().orElse(6)];
      for (int i = 0; i < faces.length; i++) {
        frequency[faces[i] - 1]++;
      }
    }

    @Override
    public int compare(Integer o1, Integer o2) {
      return Integer.compare(frequency[o1 - 1], frequency[o2 - 1]);
    }
  }

  enum Rank {
    NOTHING(0),
    PAIR(NOTHING.rank + 1),
    TWO_PAIRS(PAIR.rank + 6),
    THREE_OF_A_KIND(TWO_PAIRS.rank + 15),
    STRAIGHT(THREE_OF_A_KIND.rank + 6),
    FULL_HOUSE(STRAIGHT.rank + 2),
    FOUR_OF_A_KIND(FULL_HOUSE.rank + 15),
    FIVE_OF_A_KIND(FOUR_OF_A_KIND.rank + 6);

    Rank(int rank) {
      this.rank = rank;
    }

    private int rank;

    int getRank() {
      return rank;
    }
  }
}
