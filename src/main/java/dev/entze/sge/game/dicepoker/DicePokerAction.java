package dev.entze.sge.game.dicepoker;

import java.util.Set;

public class DicePokerAction {

  public static final DicePokerAction FOLD = new DicePokerAction(Integer.parseInt("fold", Character.MAX_RADIX));
  public static final DicePokerAction CALL = new DicePokerAction(Integer.parseInt("call", Character.MAX_RADIX));
  public static final DicePokerAction RAISE = new DicePokerAction(Integer.parseInt("raise", Character.MAX_RADIX));
  public static final DicePokerAction BET = new DicePokerAction(Integer.parseInt("bet", Character.MAX_RADIX));
  static final Set<DicePokerAction> ALLINITALBETS = Set.of(CALL, RAISE, BET);
  static final Set<DicePokerAction> ALLBETS = Set.of(FOLD, CALL, RAISE, BET);


  public static final DicePokerAction ROLL1 = new DicePokerAction(1);
  public static final DicePokerAction ROLL2 = new DicePokerAction(2);
  public static final DicePokerAction ROLL3 = new DicePokerAction(3);
  public static final DicePokerAction ROLL4 = new DicePokerAction(4);
  public static final DicePokerAction ROLL5 = new DicePokerAction(5);
  public static final DicePokerAction ROLL6 = new DicePokerAction(6);
  static final Set<DicePokerAction> ALLROLLS = Set.of(ROLL1, ROLL2, ROLL3, ROLL4, ROLL5, ROLL6);

  public static final DicePokerAction NOREROLL = new DicePokerAction(0);
  public static final DicePokerAction REROLL = new DicePokerAction(~0);

  private int id;

  private DicePokerAction(int id) {
    this.id = id;
  }


  static DicePokerAction roll(int roll) {
    return new DicePokerAction(roll);
  }

  static int extractRoll(DicePokerAction action) {
    return action.id & 0b111;
  }

  public boolean isInitialBettingAction() {
    return this.equals(CALL) || this.equals(RAISE) || this.equals(BET);
  }

  public boolean isBettingAction() {
    return this.equals(FOLD) || isInitialBettingAction();
  }

  public boolean isRollingAction() {
    return this.equals(ROLL1) || this.equals(ROLL2) || this.equals(ROLL3) || this.equals(ROLL4)
        || this.equals(ROLL5) || this.equals(ROLL6);
  }

  public boolean isRerollingAction() {
    return id == 0 || id == ~0;
  }

  public boolean isValid() {
    return isBettingAction() || isRollingAction() || isRerollingAction();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DicePokerAction that = (DicePokerAction) o;
    return id == that.id;
  }

  @Override
  public int hashCode() {
    return id;
  }

  @Override
  public String toString() {
    if (this.equals(FOLD)) {
      return "Fold";
    } else if (this.equals(CALL)) {
      return "Call";
    } else if (this.equals(RAISE)) {
      return "Raise";
    } else if (this.equals(BET)) {
      return "Bet";
    } else if (this.equals(ROLL1)) {
      return "Rolled 1";
    } else if (this.equals(ROLL2)) {
      return "Rolled 2";
    } else if (this.equals(ROLL3)) {
      return "Rolled 3";
    } else if (this.equals(ROLL4)) {
      return "Rolled 4";
    } else if (this.equals(ROLL5)) {
      return "Rolled 5";
    } else if (this.equals(ROLL6)) {
      return "Rolled 6";
    } else if (this.equals(NOREROLL)) {
      return "Don't reroll";
    } else if (this.equals(REROLL)) {
      return "Reroll";
    }
    return "INVALID";
  }
}
