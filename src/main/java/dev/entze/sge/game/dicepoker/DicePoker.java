package dev.entze.sge.game.dicepoker;

import static dev.entze.sge.game.dicepoker.DicePokerAction.NOREROLL;
import static dev.entze.sge.game.dicepoker.DicePokerAction.roll;

import at.ac.tuwien.ifs.sge.game.ActionRecord;
import at.ac.tuwien.ifs.sge.game.Game;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.Set;

public class DicePoker implements Game<DicePokerAction, DicePokerBoard> {

  public static final int SOUTH = 0;
  public static final int NORTH = 1;

  private int currentPlayerId;
  private final boolean canonical;
  private final List<ActionRecord<DicePokerAction>> actionRecords;
  private final DicePokerBoard board;

  private final Random random;

  public DicePoker() {
    this(DicePokerConfiguration.DEFAULT_DICE_POKER_CONFIG);
  }

  public DicePoker(String yaml, int numberOfPlayers) {
    this(DicePokerConfiguration.fromYaml(yaml));
    if (numberOfPlayers != 2) {
      throw new IllegalArgumentException("2 player game");
    }
  }

  public DicePoker(DicePokerConfiguration configuration) {
    this(0, true, Collections.emptyList(), new DicePokerBoard(configuration), new Random());
  }

  public DicePoker(DicePoker dicePoker) {
    this(
        dicePoker.currentPlayerId,
        dicePoker.canonical,
        dicePoker.actionRecords,
        dicePoker.board,
        dicePoker.random);
  }

  public DicePoker(
      int currentPlayerId,
      boolean canonical,
      List<ActionRecord<DicePokerAction>> actionRecords,
      DicePokerBoard board,
      Random random) {
    this.currentPlayerId = currentPlayerId;
    this.canonical = canonical;
    this.actionRecords = new ArrayList<>(actionRecords);
    this.board = new DicePokerBoard(board);
    this.random = random;
  }

  @Override
  public boolean isGameOver() {
    return board.getPot() <= 0 && (board.getSouthBalance() <= 0 || board.getNorthBalance() <= 0);
  }

  @Override
  public int getMinimumNumberOfPlayers() {
    return 2;
  }

  @Override
  public int getMaximumNumberOfPlayers() {
    return 2;
  }

  @Override
  public int getNumberOfPlayers() {
    return 2;
  }

  @Override
  public int getCurrentPlayer() {
    return currentPlayerId;
  }

  @Override
  public double getPlayerUtilityWeight(int player) {
    if (player < 0) {
      return 0;
    }
    return 1 - 2 * player;
  }

  @Override
  public double getUtilityValue(int player) {
    return player == SOUTH ? board.getSouthBalance() : board.getNorthBalance();
  }

  /*
  @Override
  public double getHeuristicValue(int player) {
    double heuristicValue = getUtilityValue(player);

    if()

    return heuristicValue;
  }

   */

  @Override
  public Set<DicePokerAction> getPossibleActions() {
    if (isGameOver()) {
      return Collections.emptySet();
    }

    if (board.haveAskedAllRerolls() && !board.areAllDiceRolled()) {
      if (!board.isBettingDone()) {
        if (!board.canBet(currentPlayerId)) {
          if (!board.canRaise(currentPlayerId)) {
            return Collections.singleton(DicePokerAction.CALL);
          }
          return Set.of(DicePokerAction.CALL, DicePokerAction.RAISE);
        }
        return DicePokerAction.ALLINITALBETS;
      }
      return DicePokerAction.ALLROLLS;
    }
    if (!board.isBettingDone()) {
      if (!board.canBet(currentPlayerId)) {
        if (!board.canRaise(currentPlayerId)) {
          return Set.of(DicePokerAction.FOLD, DicePokerAction.CALL);
        }
        return Set.of(DicePokerAction.FOLD, DicePokerAction.CALL, DicePokerAction.RAISE);
      }
      return DicePokerAction.ALLBETS;
    }

    return Set.of(NOREROLL, DicePokerAction.REROLL);
  }

  @Override
  public DicePokerBoard getBoard() {
    return new DicePokerBoard(board);
  }

  @Override
  public boolean isValidAction(DicePokerAction dicePokerAction) {
    if (isGameOver()) {
      return false;
    }
    if (board.haveAskedAllRerolls() && !board.areAllDiceRolled()) {
      if (!board.isBettingDone()) {
        return dicePokerAction.equals(DicePokerAction.BET) && board.canBet(currentPlayerId)
            || dicePokerAction.equals(DicePokerAction.RAISE) && board.canRaise(currentPlayerId)
            || dicePokerAction.equals(DicePokerAction.CALL);
      }
      return dicePokerAction.isRollingAction();
    }
    if (!board.isBettingDone()) {
      return dicePokerAction.equals(DicePokerAction.BET) && board.canBet(currentPlayerId)
          || dicePokerAction.equals(DicePokerAction.RAISE) && board.canRaise(currentPlayerId)
          || dicePokerAction.equals(DicePokerAction.CALL)
          || dicePokerAction.equals(DicePokerAction.FOLD);
    }
    return dicePokerAction.isRerollingAction();
  }

  private boolean doBet(DicePoker next, DicePokerAction dicePokerAction) {
    if (dicePokerAction.equals(DicePokerAction.BET)) {
      if (!next.board.canBet(next.currentPlayerId)) {
        throw new IllegalArgumentException("Not enough money to " + dicePokerAction.toString());
      }
      next.board.bet(next.currentPlayerId);
    } else if (dicePokerAction.equals(DicePokerAction.RAISE)) {
      if (!next.board.canRaise(next.currentPlayerId)) {
        throw new IllegalArgumentException("Not enough money to " + dicePokerAction.toString());
      }
      next.board.raise(next.currentPlayerId);
    } else {
      return false;
    }
    return true;
  }

  @Override
  public Game<DicePokerAction, DicePokerBoard> doAction(DicePokerAction dicePokerAction) {
    if (isGameOver()) {
      throw new IllegalStateException("Cannot do action when game is over");
    }

    DicePoker next = new DicePoker(this);

    if (next.board.haveAskedAllRerolls() && !next.board.areAllDiceRolled()) {
      if (!next.board.isBettingDone()) {
        if (dicePokerAction.equals(DicePokerAction.CALL)) {
          if (next.board.haveAlreadyBet()) {
            next.board.call(next.currentPlayerId);
            if (next.board.haveAgreedOnPot()) {
              next.board.finishBetting();
              next.currentPlayerId = 2;
            }
          } else {
            next.board.blind(next.currentPlayerId);
          }
        } else if (!doBet(next, dicePokerAction)) {
          throw new IllegalArgumentException(
              "Cannot " + dicePokerAction.toString() + " while initial bet");
        }
      } else {

        if (!dicePokerAction.isRollingAction()) {
          throw new IllegalArgumentException(
              dicePokerAction.toString() + " is not a valid die roll");
        }

        next.currentPlayerId =
            1 - next.board.nextDieRoll(DicePokerAction.extractRoll(dicePokerAction));
        if ((1 - next.currentPlayerId >= 0 && next.board.bothRerolled())
            || (next.board.areAllDiceRolled()
                && ((next.board.isNorthRerolled()
                        && 1 - next.currentPlayerId == SOUTH
                        && next.board.compareHands() > 0)
                    || (next.board.isSouthRerolled()
                        && 1 - next.currentPlayerId == NORTH
                        && next.board.compareHands() < 0)))) {
          next.board.awardWin();
          next.board.wrapRound();
          next.currentPlayerId = 1 - next.board.getRoundFirstPlayer();
        } else if (1 - next.currentPlayerId >= 0) {
          next.board.sortFaces();
          next.board.askDice();
          next.board.setBettingDone(next.board.isSouthRerolled() || next.board.isNorthRerolled());
        }
      }
    } else {
      if (!next.board.isBettingDone()) {
        next.board.askDice();
        if (dicePokerAction.equals(DicePokerAction.FOLD)) {
          next.board.fold();
          next.board.incWins(1 - next.currentPlayerId);
          next.board.wrapRound();
          next.currentPlayerId = 1 - next.board.getRoundFirstPlayer();
        } else if (dicePokerAction.equals(DicePokerAction.CALL)) {
          boolean firstBet =
              !next.board.haveAlreadyBet()
                  && next.board.getRoundFirstPlayer() == next.currentPlayerId;
          next.board.call(next.currentPlayerId);
          if (next.board.haveAgreedOnPot() && !(firstBet)) {
            next.board.finishBetting();
            next.currentPlayerId = 1 - next.board.getRoundFirstPlayer();
          }
        } else if (!doBet(next, dicePokerAction)) {
          throw new IllegalArgumentException("Cannot " + dicePokerAction.toString() + " while bet");
        }
      } else {
        if (!dicePokerAction.isRerollingAction()) {
          throw new IllegalArgumentException(
              dicePokerAction.toString() + " is not valid a reroll action");
        }
        next.currentPlayerId =
            1
                - next.board.selectNextDie(
                    currentPlayerId, dicePokerAction.equals(DicePokerAction.REROLL));
      }
    }

    next.currentPlayerId = 1 - next.currentPlayerId;
    next.actionRecords.add(new ActionRecord<>(currentPlayerId, dicePokerAction));
    return next;
  }

  @Override
  public DicePokerAction determineNextAction() {
    return roll(random.nextInt(6) + 1);
  }

  @Override
  public List<ActionRecord<DicePokerAction>> getActionRecords() {
    return Collections.unmodifiableList(actionRecords);
  }

  @Override
  public boolean isCanonical() {
    return canonical;
  }

  @Override
  public Game<DicePokerAction, DicePokerBoard> getGame(int player) {
    return new DicePoker(
        currentPlayerId, false, actionRecords, new DicePokerBoard(board), new Random());
  }

  @Override
  public String toString() {
    return Arrays.toString(getGameUtilityValue());
  }

  @Override
  public String toTextRepresentation() {
    StringBuilder stringBuilder = new StringBuilder("\n");
    int[] south;
    int[] north;
    if (board.wasResetLastAction()) {
      south = board.getLastSouthDiceFaces().clone();
      north = board.getLastNorthDiceFaces().clone();
      addBoard(stringBuilder, south, north, -1);
      stringBuilder.append('\n');
    }

    south = board.getSouthDiceFaces().clone();
    north = board.getNorthDiceFaces().clone();

    addBoard(stringBuilder, south, north, currentPlayerId);

    return stringBuilder.toString();
  }

  private void addBoard(
      StringBuilder stringBuilder, int[] south, int[] north, int currentPlayerId) {

    if (DicePokerUtil.isFullHand(north)) {
      addLabelOfDice(stringBuilder, DicePokerUtil.labelOfHand(north));
    }
    addEdgeOfDice(stringBuilder, north);
    stringBuilder.append('\n');
    if (currentPlayerId != NORTH || board.isNorthRerolled() || board.haveAskedAllRerolls()) {
      addFacesOfDice(stringBuilder, north);
    } else {
      addFacesOfDice(stringBuilder, north, Collections.singleton(board.getAskedDice()));
    }

    stringBuilder
        .append(" Wins: ")
        .append(board.getNorthWins())
        .append(" Balance: ")
        .append(board.getNorthBalance())
        .append(" Bet: ")
        .append(board.getNorthPot())
        .append("\n");

    addEdgeOfDice(stringBuilder, north);
    stringBuilder.append('\n');

    stringBuilder.append("          ");
    if (currentPlayerId == SOUTH) {
      stringBuilder.append("SOUTH");
    } else if (currentPlayerId == NORTH) {
      stringBuilder.append("NORTH");
    } else {
      stringBuilder.append("     ");
    }
    stringBuilder
        .append("          ")
        .append(" Pot: ")
        .append(board.getPot())
        .append(" Blind: ")
        .append(board.getBlind())
        .append('\n');

    addEdgeOfDice(stringBuilder, south);
    stringBuilder.append('\n');
    if (currentPlayerId != SOUTH || board.isSouthRerolled() || board.haveAskedAllRerolls()) {
      addFacesOfDice(stringBuilder, south);
    } else {
      addFacesOfDice(stringBuilder, south, Collections.singleton(board.getAskedDice()));
    }
    stringBuilder
        .append(" Wins: ")
        .append(board.getSouthWins())
        .append(" Balance: ")
        .append(board.getSouthBalance())
        .append(" Bet: ")
        .append(board.getSouthPot())
        .append('\n');

    addEdgeOfDice(stringBuilder, south);
    stringBuilder.append('\n');

    if (DicePokerUtil.isFullHand(south)) {
      addLabelOfDice(stringBuilder, DicePokerUtil.labelOfHand(south));
    }
    stringBuilder.append('\n');
  }

  private void addLabelOfDice(StringBuilder stringBuilder, String label) {
    int pre = 12;
    int post = 13;
    while (pre + post + (label != null ? label.length() : 0) > 25) {
      if (pre < post) {
        post--;
      } else {
        pre--;
      }
    }

    for (int i = 0; i < pre; i++) {
      stringBuilder.append(' ');
    }
    if (label != null) {
      stringBuilder.append(label);
    }
    stringBuilder.append('\n');
  }

  private void addEdgeOfDice(StringBuilder stringBuilder, int[] faces) {
    for (int f : faces) {
      if (1 <= f && f <= 6) {
        stringBuilder.append("+---+");
      } else {
        stringBuilder.append("     ");
      }
    }
  }

  private void addFacesOfDice(StringBuilder stringBuilder, int[] faces) {
    for (int f : faces) {
      if (1 <= f && f <= 6) {
        stringBuilder.append("| ").append(f).append(" |");
      } else {
        stringBuilder.append("     ");
      }
    }
  }

  private void addFacesOfDice(StringBuilder stringBuilder, int[] faces, Set<Integer> mark) {
    for (int i = 0; i < faces.length; i++) {
      int f = faces[i];
      if (1 <= f && f <= 6) {
        stringBuilder.append('|');
        if (mark.contains(i)) {
          stringBuilder.append('(');
        } else {
          stringBuilder.append(' ');
        }
        stringBuilder.append(f);
        if (mark.contains(i)) {
          stringBuilder.append(')');
        } else {
          stringBuilder.append(' ');
        }
        stringBuilder.append('|');
      } else {
        stringBuilder.append("     ");
      }
    }
  }
}
