package dev.entze.sge.game.dicepoker;

import org.yaml.snakeyaml.Yaml;

public class DicePokerConfiguration {

  public static final DicePokerConfiguration DEFAULT_DICE_POKER_CONFIG =
      new DicePokerConfiguration(2, 0, 50, 50, 5);


  private int games = 2;
  private int difference = 0;

  private int southBalance = 50;
  private int northBalance = 50;

  private int blind = 5;

  public DicePokerConfiguration() {
    this(2, 0, 50, 50, 5);
  }

  public DicePokerConfiguration(int games, int difference,
      int southBalance, int northBalance, int blind) {
    this.games = games;
    this.difference = difference;
    this.southBalance = southBalance;
    this.northBalance = northBalance;
    this.blind = blind;
  }

  public int getGames() {
    return games;
  }

  public void setGames(int games) {
    this.games = games;
  }

  public int getDifference() {
    return difference;
  }

  public void setDifference(int difference) {
    this.difference = difference;
  }

  public int getSouthBalance() {
    return southBalance;
  }

  public void setSouthBalance(int southBalance) {
    this.southBalance = southBalance;
  }

  public int getNorthBalance() {
    return northBalance;
  }

  public void setNorthBalance(int northBalance) {
    this.northBalance = northBalance;
  }

  public int getBlind() {
    return blind;
  }

  public void setBlind(int blind) {
    this.blind = blind;
  }

  public void setBalance(int bank) {
    this.southBalance = bank;
    this.northBalance = bank;
  }

  public static DicePokerConfiguration fromYaml(String yaml) {
    if (yaml == null) {
      return new DicePokerConfiguration();
    }
    return new Yaml().load(yaml);
  }

}
