package dev.entze.sge.game.dicepoker;

import at.ac.tuwien.ifs.sge.util.Util;
import java.util.Arrays;
import java.util.Comparator;

public class DicePokerBoard {

  // settings
  private final int games;
  private final int difference;

  // actual board
  private int southBalance;
  private int northBalance;

  private final int blind;

  private final int[] southDiceFaces;
  private final int[] northDiceFaces;

  private final int[] lastSouthDiceFaces;
  private final int[] lastNorthDiceFaces;

  private int askedDice;
  private boolean southRerolled;
  private boolean northRerolled;

  private int southWins;
  private int northWins;

  private boolean bettingDone;

  private int pot;
  private int southPot;
  private int northPot;

  private int roundFirstPlayer;
  private int gameFirstPlayer;

  private boolean wasResetLastAction;

  public DicePokerBoard(DicePokerConfiguration configuration) {
    games = configuration.getGames();
    difference = configuration.getDifference();
    southBalance = configuration.getSouthBalance();
    northBalance = configuration.getNorthBalance();
    blind = configuration.getBlind();
    southDiceFaces = new int[5];
    northDiceFaces = new int[5];
    lastSouthDiceFaces = new int[5];
    lastNorthDiceFaces = new int[5];
    askedDice = southDiceFaces.length;
    southRerolled = northRerolled = false;
    southWins = northWins = 0;
    bettingDone = false;
    pot = 0;
    southPot = northPot = 0;
    gameFirstPlayer = roundFirstPlayer = 0;
    wasResetLastAction = false;
  }

  public DicePokerBoard(DicePokerBoard dicePokerBoard) {
    this(
        dicePokerBoard.games,
        dicePokerBoard.difference,
        dicePokerBoard.southBalance,
        dicePokerBoard.northBalance,
        dicePokerBoard.blind,
        dicePokerBoard.southDiceFaces,
        dicePokerBoard.northDiceFaces,
        dicePokerBoard.lastSouthDiceFaces,
        dicePokerBoard.lastNorthDiceFaces,
        dicePokerBoard.askedDice,
        dicePokerBoard.southRerolled,
        dicePokerBoard.northRerolled,
        dicePokerBoard.southWins,
        dicePokerBoard.northWins,
        dicePokerBoard.bettingDone,
        dicePokerBoard.pot,
        dicePokerBoard.southPot,
        dicePokerBoard.northPot,
        dicePokerBoard.roundFirstPlayer,
        dicePokerBoard.gameFirstPlayer,
        dicePokerBoard.wasResetLastAction);
  }

  public DicePokerBoard(
      int games,
      int difference,
      int southBalance,
      int northBalance,
      int blind,
      int[] southDiceFaces,
      int[] northDiceFaces,
      int[] lastSouthDiceFaces,
      int[] lastNorthDiceFaces,
      int askedDice,
      boolean southRerolled,
      boolean northRerolled,
      int southWins,
      int northWins,
      boolean bettingDone,
      int pot,
      int southPot,
      int northPot,
      int roundFirstPlayer,
      int gameFirstPlayer,
      boolean wasResetLastAction) {
    this.games = games;
    this.difference = difference;
    this.southBalance = southBalance;
    this.northBalance = northBalance;
    this.blind = blind;
    this.southDiceFaces = southDiceFaces.clone();
    this.northDiceFaces = northDiceFaces.clone();
    this.askedDice = askedDice;
    this.southRerolled = southRerolled;
    this.northRerolled = northRerolled;
    this.lastSouthDiceFaces = lastSouthDiceFaces.clone();
    this.lastNorthDiceFaces = lastNorthDiceFaces.clone();
    this.southWins = southWins;
    this.northWins = northWins;
    this.bettingDone = bettingDone;
    this.pot = pot;
    this.southPot = southPot;
    this.northPot = northPot;
    this.roundFirstPlayer = roundFirstPlayer;
    this.gameFirstPlayer = gameFirstPlayer;
    this.wasResetLastAction = wasResetLastAction;
  }

  public int getSouthBalance() {
    return southBalance + southPot;
  }

  public int getNorthBalance() {
    return northBalance + northPot;
  }

  int[] getSouthDiceFaces() {
    return southDiceFaces;
  }

  int[] getNorthDiceFaces() {
    return northDiceFaces;
  }

  public int getBlind() {
    return blind;
  }

  public int getPot() {
    return pot;
  }

  public int getSouthWins() {
    return southWins;
  }

  public int getNorthWins() {
    return northWins;
  }

  int getRoundFirstPlayer() {
    return roundFirstPlayer;
  }

  boolean isBettingDone() {
    return bettingDone;
  }

  int getSouthPot() {
    return southPot;
  }

  int getNorthPot() {
    return northPot;
  }

  int getAskedDice() {
    return askedDice;
  }

  int[] getLastSouthDiceFaces() {
    return lastSouthDiceFaces;
  }

  int[] getLastNorthDiceFaces() {
    return lastNorthDiceFaces;
  }

  boolean wasResetLastAction() {
    return wasResetLastAction;
  }

  public void setBettingDone(boolean bettingDone) {
    this.bettingDone = bettingDone;
  }

  private void swapRoundFirstPlayer() {
    roundFirstPlayer = 1 - roundFirstPlayer;
  }

  private void swapGameFirstPlayer() {
    gameFirstPlayer = 1 - gameFirstPlayer;
    roundFirstPlayer = gameFirstPlayer;
  }

  void incWins(int player) {
    if (player == DicePoker.SOUTH) {
      southWins++;
    } else if (player == DicePoker.NORTH) {
      northWins++;
    }
  }

  private void resetDice() {
    wasResetLastAction = true;
    System.arraycopy(southDiceFaces, 0, lastSouthDiceFaces, 0, southDiceFaces.length);
    System.arraycopy(northDiceFaces, 0, lastNorthDiceFaces, 0, northDiceFaces.length);

    Arrays.fill(southDiceFaces, 0);
    Arrays.fill(northDiceFaces, 0);
  }

  private boolean areAllDiceRolled(int[] faces) {
    return Arrays.stream(faces).allMatch(f -> 1 <= f && f <= 6);
  }

  boolean areAllDiceRolled() {
    return areAllDiceRolled(southDiceFaces) && areAllDiceRolled(northDiceFaces);
  }

  boolean haveAskedAllRerolls() {
    return askedDice >= southDiceFaces.length;
  }

  void blind(int player) {
    wasResetLastAction = false;
    if (player == DicePoker.SOUTH) {
      southBalance -= blind;
      southPot += blind;
    } else if (player == DicePoker.NORTH) {
      northBalance -= blind;
      northPot += blind;
    }
  }

  boolean canBlind(int player) {
    return (player != DicePoker.SOUTH || southBalance >= blind)
        && (player != DicePoker.NORTH || northBalance >= blind);
  }

  boolean canCall(int player) {
    return canPlayMoney(player, 0);
  }

  boolean canRaise(int player) {
    return canPlayMoney(player, 1 + (haveAlreadyBet() ? 0 : 1));
  }

  boolean canBet(int player) {
    return canPlayMoney(player, 2 + (haveAlreadyBet() ? 0 : 1));
  }

  private boolean canPlayMoney(int player, int multiplier) {
    return (player != DicePoker.SOUTH
            || ((southBalance >= Math.max(0, northPot - southPot) + blind * multiplier)
                && (northBalance >= blind * multiplier)))
        && (player != DicePoker.NORTH
            || ((northBalance >= Math.max(0, southPot - northPot) + blind * multiplier)
                && (southBalance >= blind * multiplier)));
  }

  void fold() {
    wasResetLastAction = false;
    southBalance += southPot;
    northBalance += northPot;
    southPot = 0;
    northPot = 0;
  }

  void call(int player) {
    playMoney(player, 0);
  }

  void raise(int player) {
    playMoney(player, 1 + (haveAlreadyBet() ? 0 : 1));
  }

  void bet(int player) {
    playMoney(player, 2 + (haveAlreadyBet() ? 0 : 1));
  }

  private void playMoney(int player, int multiplier) {
    wasResetLastAction = false;
    int difference;
    if (player == DicePoker.SOUTH) {
      difference = Math.max(0, northPot - southPot);
      southBalance -= difference + blind * multiplier;
      pot += southPot + difference + northPot;
      northPot = 0;
      southPot = blind * multiplier;
    } else if (player == DicePoker.NORTH) {
      difference = Math.max(0, southPot - northPot);
      northBalance -= difference + blind * multiplier;
      pot += northPot + difference + southPot;
      southPot = 0;
      northPot += blind * multiplier;
    }
  }

  void askDice() {
    askedDice = 0;
  }

  boolean haveAlreadyBet() {
    return southPot > 0 || northPot > 0;
  }

  boolean haveAgreedOnPot() {
    return southPot == northPot;
  }

  void finishBetting() {
    wasResetLastAction = false;
    bettingDone = true;
    pot += southPot + northPot;
    southPot = 0;
    northPot = 0;
  }

  int nextDieRoll(int face) {
    int nextPlayer =
        southRerolled ? DicePoker.NORTH : northRerolled ? DicePoker.SOUTH : roundFirstPlayer;
    if (1 <= face && face <= 6) {

      for (int i = 0; i < southDiceFaces.length; i++) {
        if (!(1 <= southDiceFaces[i] && southDiceFaces[i] <= 6)) {
          southDiceFaces[i] = face;
          return (areAllDiceRolled() ? nextPlayer : -1);
        }
      }

      for (int i = 0; i < northDiceFaces.length; i++) {
        if (!(1 <= northDiceFaces[i] && northDiceFaces[i] <= 6)) {
          northDiceFaces[i] = face;
          return (areAllDiceRolled() ? nextPlayer : -1);
        }
      }
    }
    return nextPlayer;
  }

  private boolean isGameOver() {
    return Math.abs(southWins - northWins) > difference && southWins >= games || northWins >= games;
  }

  private void awardPot() {
    if (southWins > northWins) {
      southBalance += pot;
    } else if (southWins < northWins) {
      northBalance += pot;
    } else {
      southBalance += pot / 2;
      northBalance += pot / 2;
    }
    pot = 0;
  }

  void wrapRound() {
    southRerolled = false;
    northRerolled = false;
    bettingDone = false;
    askedDice = southDiceFaces.length;
    resetDice();
    swapRoundFirstPlayer();
    if (isGameOver() || !canStillPlay(southBalance) || !canStillPlay(northBalance)) {
      awardPot();
      swapGameFirstPlayer();
      southWins = 0;
      northWins = 0;
    }
  }

  private boolean canStillPlay(int balance) {
    return balance
        >= Math.max(
                1,
                Math.max(0, games - southWins + northWins)
                    + Math.max(0, difference - Math.abs(southWins - northWins)))
            * blind;
  }

  int selectNextDie(int player, boolean select) {
    if (select) {
      if (player == DicePoker.SOUTH) {
        southDiceFaces[askedDice] = 0;
      } else if (player == DicePoker.NORTH) {
        northDiceFaces[askedDice] = 0;
      }
    }
    askedDice++;
    if (haveAskedAllRerolls()) {
      if (player == DicePoker.SOUTH) {
        southRerolled = true;
        if (areAllDiceRolled(southDiceFaces)) {
          askDice();
          return DicePoker.NORTH;
        }
      } else if (player == DicePoker.NORTH) {
        northRerolled = true;
        if (areAllDiceRolled(northDiceFaces)) {
          askDice();
          return DicePoker.SOUTH;
        }
      }
      return -1;
    }
    return player;
  }

  boolean isSouthRerolled() {
    return southRerolled;
  }

  boolean isNorthRerolled() {
    return northRerolled;
  }

  boolean bothRerolled() {
    return isSouthRerolled() && isNorthRerolled();
  }

  int compareHands() {
    return Integer.compare(
        DicePokerUtil.rankingOfHand(southDiceFaces), DicePokerUtil.rankingOfHand(northDiceFaces));
  }

  void awardWin() {
    int compare = compareHands();
    if (compare >= 0) {
      southWins++;
    }
    if (compare <= 0) {
      northWins++;
    }
  }

  void sortFaces() {
    if (areAllDiceRolled(southDiceFaces)) {
      Util.sort(
          southDiceFaces,
          (DicePokerUtil.getDiceFaceComparator(southDiceFaces)
                  .thenComparing(Comparator.naturalOrder()))
              .reversed());
    }
    if (areAllDiceRolled(northDiceFaces)) {
      Util.sort(
          northDiceFaces,
          (DicePokerUtil.getDiceFaceComparator(northDiceFaces)
                  .thenComparing(Comparator.naturalOrder()))
              .reversed());
    }
  }
}
