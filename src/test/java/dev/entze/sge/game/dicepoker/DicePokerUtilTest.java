package dev.entze.sge.game.dicepoker;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class DicePokerUtilTest {

  @Test
  public void test_rankingOfHand_Nothing_Nothing() {

    assertEquals(
        "Both Busts/Nothings have the same rank.",
        DicePokerUtil.rankingOfHand(new int[] {1, 2, 3, 4, 6}),
        DicePokerUtil.rankingOfHand(new int[] {6, 5, 4, 3, 1}));
  }

  @Test
  public void test_rankingOfHand_Pair_FiveOfAKind() {
    assertTrue(
        "Pair is less than a Five of a Kind.",
        DicePokerUtil.rankingOfHand(new int[] {1, 1, 3, 4, 6})
            < DicePokerUtil.rankingOfHand(new int[] {6, 6, 6, 6, 6}));
  }

  @Test
  public void test_rankingOfHand_ThreeOfAKind_FourOfAKind() {
    assertTrue(
        "Three of a Kind is less than a Four of a Kind.",
        DicePokerUtil.rankingOfHand(new int[] {3, 3, 3, 4, 6})
            < DicePokerUtil.rankingOfHand(new int[] {6, 6, 6, 6, 5}));
  }

  @Test
  public void test_rankingOfHand_Straight_FullHouse() {
    assertTrue(
        "Straight is less than a Full House.",
        DicePokerUtil.rankingOfHand(new int[] {1, 2, 3, 4, 5})
            < DicePokerUtil.rankingOfHand(new int[] {6, 6, 6, 5, 5}));
  }

  @Test
  public void test_rankingOfHand_TwoPairs_TwoPairs() {
    assertEquals(
        "Two equal Two Pairs are equal.",
        DicePokerUtil.rankingOfHand(new int[] {2, 2, 5, 5, 1}),
        DicePokerUtil.rankingOfHand(new int[] {1, 5, 2, 2, 5}));
  }

  @Test
  public void test_rankingOfHand_Pair_Pair() {
    assertTrue(
        "Two 1s are less than Two 3s.",
        DicePokerUtil.rankingOfHand(new int[] {1, 1, 2, 4, 5})
            < DicePokerUtil.rankingOfHand(new int[] {3, 1, 2, 3, 4}));
  }

  @Test(expected = IllegalArgumentException.class)
  public void error_rankingOfHand_NotEnoughDice() {
    DicePokerUtil.rankingOfHand(new int[] {1});
  }

  @Test(expected = IllegalArgumentException.class)
  public void error_rankingOfHand_TooManyDice() {
    DicePokerUtil.rankingOfHand(new int[] {1, 2, 3, 4, 5, 6});
  }

  @Test(expected = IllegalArgumentException.class)
  public void error_rankingOfHand_InvalidDice() {
    DicePokerUtil.rankingOfHand(new int[] {-1, 7, 3, 4, 5});
  }

  @Test(expected = IllegalArgumentException.class)
  public void error_rankingOfHand_NotCorrectNumberOfDice_InvalidDice() {
    DicePokerUtil.rankingOfHand(new int[] {-1});
  }

  @Test
  public void test_labelOfHand_Nothing() {
    assertEquals("Nothing", DicePokerUtil.labelOfHand(new int[] {6, 5, 4, 3, 1}));
  }

  @Test
  public void test_labelOfHand_Pair() {
    assertEquals("1 Pair", DicePokerUtil.labelOfHand(new int[] {1, 1, 3, 4, 6}));
  }

  @Test
  public void test_labelOfHand_TwoPair_1() {
    assertEquals("6, 5 Two Pair", DicePokerUtil.labelOfHand(new int[] {6, 6, 5, 5, 3}));
  }

  @Test
  public void test_labelOfHand_TwoPair_2() {
    assertEquals("3, 1 Two Pair", DicePokerUtil.labelOfHand(new int[] {1, 1, 3, 3, 6}));
  }

  @Test
  public void test_labelOfHand_ThreeOfAKind() {
    assertEquals("3 Three of a Kind", DicePokerUtil.labelOfHand(new int[] {3, 3, 3, 4, 6}));
  }

  @Test
  public void test_labelOfHand_Straight() {
    assertEquals("5-high Straight", DicePokerUtil.labelOfHand(new int[] {1, 2, 3, 4, 5}));
  }

  @Test
  public void test_labelOfHand_FullHouse() {
    assertEquals("3, 2 Full House", DicePokerUtil.labelOfHand(new int[] {2, 3, 2, 3, 3}));
  }

  @Test
  public void test_labelOfHand_FourOfAKind() {
    assertEquals("1 Four of a Kind", DicePokerUtil.labelOfHand(new int[] {1, 1, 1, 1, 3}));
  }

  @Test
  public void test_labelOfHand_FiveOfAKind() {
    assertEquals("6 Five of a Kind", DicePokerUtil.labelOfHand(new int[] {6, 6, 6, 6, 6}));
  }

  @Test(expected = IllegalArgumentException.class)
  public void error_labelOfHand_NotEnoughDice() {
    DicePokerUtil.labelOfHand(new int[] {1});
  }

  @Test(expected = IllegalArgumentException.class)
  public void error_labelOfHand_TooManyDice() {
    DicePokerUtil.labelOfHand(new int[] {1, 2, 3, 4, 5, 6});
  }

  @Test(expected = IllegalArgumentException.class)
  public void error_labelOfHand_InvalidDice() {
    DicePokerUtil.labelOfHand(new int[] {-1, 7, 3, 4, 5});
  }

  @Test(expected = IllegalArgumentException.class)
  public void error_labelOfHand_NotCorrectNumberOfDice_InvalidDice() {
    DicePokerUtil.labelOfHand(new int[] {-1});
  }
}
