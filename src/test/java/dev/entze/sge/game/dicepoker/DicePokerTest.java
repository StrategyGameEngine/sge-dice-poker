package dev.entze.sge.game.dicepoker;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import at.ac.tuwien.ifs.sge.game.Dice;
import java.util.Set;
import org.junit.Test;

public class DicePokerTest {

  DicePoker dicePoker;

  @Test
  public void test_game_1() {
    dicePoker = new DicePoker();

    //INITIAL BET
    assertFalse(dicePoker.isGameOver());
    assertEquals(0L, (long) dicePoker.getUtilityValue());
    assertEquals(50L, (long) dicePoker.getUtilityValue(DicePoker.SOUTH));
    assertEquals(50L, (long) dicePoker.getUtilityValue(DicePoker.NORTH));
    assertEquals(DicePokerAction.ALLINITALBETS, dicePoker.getPossibleActions());
    assertEquals(DicePoker.SOUTH, dicePoker.getCurrentPlayer());
    dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.CALL);
    assertFalse(dicePoker.isGameOver());
    assertEquals(DicePokerAction.ALLINITALBETS, dicePoker.getPossibleActions());
    assertEquals(0L, (long) dicePoker.getUtilityValue());
    assertEquals(50L, (long) dicePoker.getUtilityValue(DicePoker.SOUTH));
    assertEquals(50L, (long) dicePoker.getUtilityValue(DicePoker.NORTH));
    assertEquals(DicePoker.NORTH, dicePoker.getCurrentPlayer());
    dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.CALL);
    assertEquals(45L, (long) dicePoker.getUtilityValue(DicePoker.SOUTH));
    assertEquals(45L, (long) dicePoker.getUtilityValue(DicePoker.NORTH));

    //ROLLS
    for (int i = 0; i < 5; i++) {
      assertFalse(dicePoker.isGameOver());
      assertEquals(DicePokerAction.ALLROLLS, dicePoker.getPossibleActions());
      assertEquals(0L, (long) dicePoker.getUtilityValue());
      assertTrue(dicePoker.getCurrentPlayer() < 0);
      dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.ROLL6);
    }
    for (int i = 0; i < 5; i++) {
      assertFalse(dicePoker.isGameOver());
      assertEquals(DicePokerAction.ALLROLLS, dicePoker.getPossibleActions());
      assertEquals(0L, (long) dicePoker.getUtilityValue());
      assertTrue(dicePoker.getCurrentPlayer() < 0);
      dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.ROLL1);
    }

    //BET
    assertEquals(0L, (long) dicePoker.getUtilityValue());
    assertEquals(DicePokerAction.ALLBETS, dicePoker.getPossibleActions());
    assertEquals(DicePoker.SOUTH, dicePoker.getCurrentPlayer());
    assertEquals(45L, (long) dicePoker.getUtilityValue(DicePoker.SOUTH));
    assertEquals(45L, (long) dicePoker.getUtilityValue(DicePoker.NORTH));
    dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.CALL);
    assertEquals(45L, (long) dicePoker.getUtilityValue(DicePoker.SOUTH));
    assertEquals(45L, (long) dicePoker.getUtilityValue(DicePoker.NORTH));
    assertFalse(dicePoker.isGameOver());
    assertEquals(DicePokerAction.ALLBETS, dicePoker.getPossibleActions());
    assertEquals(0L, (long) dicePoker.getUtilityValue());
    assertEquals(DicePoker.NORTH, dicePoker.getCurrentPlayer());
    assertEquals(45L, (long) dicePoker.getUtilityValue(DicePoker.SOUTH));
    assertEquals(45L, (long) dicePoker.getUtilityValue(DicePoker.NORTH));
    dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.FOLD);
    assertEquals(45L, (long) dicePoker.getUtilityValue(DicePoker.SOUTH));
    assertEquals(45L, (long) dicePoker.getUtilityValue(DicePoker.NORTH));

    //INITIAL_BET
    assertFalse(dicePoker.isGameOver());
    assertEquals(0L, (long) dicePoker.getUtilityValue());
    assertEquals(DicePokerAction.ALLINITALBETS, dicePoker.getPossibleActions());
    assertEquals(DicePoker.NORTH, dicePoker.getCurrentPlayer());
    assertEquals(45L, (long) dicePoker.getUtilityValue(DicePoker.SOUTH));
    assertEquals(45L, (long) dicePoker.getUtilityValue(DicePoker.NORTH));
    dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.CALL);
    assertEquals(45L, (long) dicePoker.getUtilityValue(DicePoker.SOUTH));
    assertEquals(45L, (long) dicePoker.getUtilityValue(DicePoker.NORTH));
    assertFalse(dicePoker.isGameOver());
    assertEquals(DicePokerAction.ALLINITALBETS, dicePoker.getPossibleActions());
    assertEquals(0L, (long) dicePoker.getUtilityValue());
    assertEquals(DicePoker.SOUTH, dicePoker.getCurrentPlayer());
    assertEquals(45L, (long) dicePoker.getUtilityValue(DicePoker.SOUTH));
    assertEquals(45L, (long) dicePoker.getUtilityValue(DicePoker.NORTH));
    dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.CALL);
    assertEquals(40L, (long) dicePoker.getUtilityValue(DicePoker.SOUTH));
    assertEquals(40L, (long) dicePoker.getUtilityValue(DicePoker.NORTH));

    //ROLLS
    for (int i = 0; i < 4; i++) {
      assertFalse(dicePoker.isGameOver());
      assertEquals(DicePokerAction.ALLROLLS, dicePoker.getPossibleActions());
      assertEquals(0L, (long) dicePoker.getUtilityValue());
      assertTrue(dicePoker.getCurrentPlayer() < 0);
      dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.ROLL5);
    }
    assertFalse(dicePoker.isGameOver());
    assertEquals(DicePokerAction.ALLROLLS, dicePoker.getPossibleActions());
    assertEquals(0L, (long) dicePoker.getUtilityValue());
    assertTrue(dicePoker.getCurrentPlayer() < 0);
    dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.ROLL1);
    for (int i = 0; i < 5; i++) {
      assertFalse(dicePoker.isGameOver());
      assertEquals(DicePokerAction.ALLROLLS, dicePoker.getPossibleActions());
      assertEquals(0L, (long) dicePoker.getUtilityValue());
      assertTrue(dicePoker.getCurrentPlayer() < 0);
      dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.ROLL1);
    }

    //BET
    assertFalse(dicePoker.isGameOver());
    assertEquals(0L, (long) dicePoker.getUtilityValue());
    assertEquals(DicePokerAction.ALLBETS, dicePoker.getPossibleActions());
    assertEquals(DicePoker.NORTH, dicePoker.getCurrentPlayer());
    assertEquals(40L, (long) dicePoker.getUtilityValue(DicePoker.SOUTH));
    assertEquals(40L, (long) dicePoker.getUtilityValue(DicePoker.NORTH));
    dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.CALL);
    assertEquals(40L, (long) dicePoker.getUtilityValue(DicePoker.SOUTH));
    assertEquals(40L, (long) dicePoker.getUtilityValue(DicePoker.NORTH));
    assertFalse(dicePoker.isGameOver());
    assertEquals(DicePokerAction.ALLBETS, dicePoker.getPossibleActions());
    assertEquals(0L, (long) dicePoker.getUtilityValue());
    assertEquals(DicePoker.SOUTH, dicePoker.getCurrentPlayer());
    assertEquals(40L, (long) dicePoker.getUtilityValue(DicePoker.SOUTH));
    assertEquals(40L, (long) dicePoker.getUtilityValue(DicePoker.NORTH));
    dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.CALL);
    assertEquals(40L, (long) dicePoker.getUtilityValue(DicePoker.SOUTH));
    assertEquals(40L, (long) dicePoker.getUtilityValue(DicePoker.NORTH));

    //REROLL
    for (int i = 0; i < 5; i++) {
      assertFalse(dicePoker.isGameOver());
      assertEquals(0L, (long) dicePoker.getUtilityValue());
      assertEquals(Set.of(DicePokerAction.NOREROLL, DicePokerAction.REROLL),
          dicePoker.getPossibleActions());
      assertEquals(DicePoker.NORTH, dicePoker.getCurrentPlayer());
      dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.NOREROLL);
    }

    for (int i = 0; i < 4; i++) {
      assertFalse(dicePoker.isGameOver());
      assertEquals(0L, (long) dicePoker.getUtilityValue());
      assertEquals(Set.of(DicePokerAction.NOREROLL, DicePokerAction.REROLL),
          dicePoker.getPossibleActions());
      assertEquals(DicePoker.SOUTH, dicePoker.getCurrentPlayer());
      dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.NOREROLL);
    }

    assertFalse(dicePoker.isGameOver());
    assertEquals(0L, (long) dicePoker.getUtilityValue());
    assertEquals(Set.of(DicePokerAction.NOREROLL, DicePokerAction.REROLL),
        dicePoker.getPossibleActions());
    assertEquals(DicePoker.SOUTH, dicePoker.getCurrentPlayer());
    dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.REROLL);

    //ROLL
    assertFalse(dicePoker.isGameOver());
    assertEquals(0L, (long) dicePoker.getUtilityValue());
    assertEquals(DicePokerAction.ALLROLLS,
        dicePoker.getPossibleActions());
    assertTrue(dicePoker.getCurrentPlayer() < 0);
    dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.ROLL5);
    assertFalse(dicePoker.isGameOver());
    assertEquals(20L, (long) dicePoker.getUtilityValue());
    assertEquals(DicePokerAction.ALLINITALBETS,
        dicePoker.getPossibleActions());


  }

  @Test
  public void test_game_2() {

    dicePoker = new DicePoker("!!dev.entze.sge.game.dicepoker.DicePokerConfiguration {"
        + " blind: 5,"
        + " difference: 0,"
        + " games: 1,"
        + " balance: 5"
        + "}", 2);

    assertEquals(Set.of(DicePokerAction.CALL), dicePoker.getPossibleActions());
    dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.CALL);
    assertEquals(Set.of(DicePokerAction.CALL), dicePoker.getPossibleActions());
    dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.CALL);
    assertEquals(0, dicePoker.getBoard().getSouthBalance());
    assertEquals(0, dicePoker.getBoard().getNorthBalance());
    assertEquals(DicePokerAction.ALLROLLS, dicePoker.getPossibleActions());
    dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.ROLL1);
    dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.ROLL2);
    dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.ROLL3);
    dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.ROLL5);
    dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.ROLL6);

    dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.ROLL3);
    dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.ROLL2);
    dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.ROLL2);
    dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.ROLL4);
    dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.ROLL2);
    assertEquals(Set.of(DicePokerAction.CALL, DicePokerAction.FOLD),
        dicePoker.getPossibleActions());
    dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.CALL);
    assertEquals(0, dicePoker.getBoard().getSouthBalance());
    assertEquals(0, dicePoker.getBoard().getNorthBalance());
    assertEquals(Set.of(DicePokerAction.CALL, DicePokerAction.FOLD),
        dicePoker.getPossibleActions());
    dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.CALL);
    assertEquals(0, dicePoker.getBoard().getSouthBalance());
    assertEquals(0, dicePoker.getBoard().getNorthBalance());
    assertEquals(Set.of(DicePokerAction.REROLL, DicePokerAction.NOREROLL),
        dicePoker.getPossibleActions());
    dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.REROLL);
    dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.REROLL);
    dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.REROLL);
    dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.REROLL);
    dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.NOREROLL);

    dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.ROLL4);
    dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.ROLL3);
    dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.ROLL5);
    dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.ROLL4);

    assertTrue(dicePoker.isGameOver());

  }

  @Test
  public void test_game_3() {
    dicePoker = new DicePoker("!!dev.entze.sge.game.dicepoker.DicePokerConfiguration {"
        + " blind: 5,"
        + " difference: 0,"
        + " games: 1,"
        + " balance: 20"
        + "}", 2);

    assertEquals(DicePokerAction.ALLINITALBETS, dicePoker.getPossibleActions());
    dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.CALL);
    assertEquals(DicePokerAction.ALLINITALBETS, dicePoker.getPossibleActions());
    dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.CALL);
    assertEquals(15, dicePoker.getBoard().getSouthBalance());
    assertEquals(15, dicePoker.getBoard().getNorthBalance());
    assertEquals(DicePokerAction.ALLROLLS, dicePoker.getPossibleActions());
    dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.ROLL1);
    dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.ROLL2);
    dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.ROLL3);
    dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.ROLL5);
    dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.ROLL6);

    dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.ROLL3);
    dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.ROLL2);
    dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.ROLL2);
    dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.ROLL4);
    dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.ROLL2);

    assertEquals(DicePokerAction.ALLBETS, dicePoker.getPossibleActions());
    dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.CALL);
    assertEquals(15, dicePoker.getBoard().getSouthBalance());
    assertEquals(15, dicePoker.getBoard().getNorthBalance());

    assertEquals(DicePokerAction.ALLBETS, dicePoker.getPossibleActions());
    dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.BET);
    assertEquals(15, dicePoker.getBoard().getSouthBalance());
    assertEquals(15, dicePoker.getBoard().getNorthBalance());

    assertEquals(Set.of(DicePokerAction.CALL, DicePokerAction.FOLD),
        dicePoker.getPossibleActions());
    dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.CALL);
    assertEquals(0, dicePoker.getBoard().getSouthBalance());
    assertEquals(0, dicePoker.getBoard().getNorthBalance());

    assertEquals(Set.of(DicePokerAction.REROLL, DicePokerAction.NOREROLL),
        dicePoker.getPossibleActions());
    dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.NOREROLL);
    dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.REROLL);
    dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.REROLL);
    dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.REROLL);
    dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.REROLL);

    dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.ROLL6);
    dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.ROLL6);
    dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.ROLL1);
    dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.ROLL4);

    dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.NOREROLL);
    dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.NOREROLL);
    dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.NOREROLL);
    dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.REROLL);
    dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.REROLL);

    dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.ROLL4);
    dicePoker = (DicePoker) dicePoker.doAction(DicePokerAction.ROLL1);

    assertTrue(dicePoker.isGameOver());
    assertEquals(40, dicePoker.getBoard().getSouthBalance());
    assertEquals(0, dicePoker.getBoard().getNorthBalance());

  }

}



